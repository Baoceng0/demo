package com.example.demo.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public Object greeting123(@RequestParam(value = "name", defaultValue = "World") String name) {

		return new Object();
	}

	@GetMapping("/hello")
	public String hello123(@RequestParam(value = "name", defaultValue = "World") String name) {
		StringBuffer sb = new StringBuffer();
		if(1 == 1){
			sb.append("test 1 == 1:" + name);
		}
		sb.append("hello world" + name);
		return sb.toString();
	}
}
