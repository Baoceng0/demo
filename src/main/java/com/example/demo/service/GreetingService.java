package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

public class GreetingService {
    public String descTaskDetail(String taskId, long type) {
        Map<String, String> map =new HashMap<>();
        map.put("AppId", "app01");
        map.put("SecretId", "SecretId01");
        map.put("SecretKey", "SecretKey01");
        map.put("RegionName", "RegionName01");
        map.put("aaa", "test003");

        //初始化cosClient
        COSClient cosClient = CosUtils.getCOSClient(map.get("AppId"),
                map.get("SecretId"),
                map.get("SecretKey"),
                map.get("RegionName"));
        Map<String, String> tdMap = cosClient.getTaskDetail();

        map.put("bbb", "456");

        return tdMap.get("TaskDetail");
    }
}
