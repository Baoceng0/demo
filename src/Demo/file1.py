import jieba

text = "绿子在电话的另一头久久默然不语，如同全世界的细雨落在全世界所有的草坪上一般的沉默在持续。"
seg_list = jieba.lcut(text,  cut_all=False, HMM=True)
print("分词结果：" + " ".join(seg_list))

stopwords = []
with open('cn_stopwords.txt',encoding='utf-8') as f:
    stopwords = f.read()

new_text = []
for w in seg_list:
    if w not in stopwords:
        new_text.append(w)

print("中文jieba分词…………")
print('去除停用词后：' + .join(new_text))