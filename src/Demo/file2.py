import nltk
# from nltk.tokenize import word_tokenize
from nltk import word_tokenize,pos_tag   #分词、词性标注
from nltk.corpus import stopwords    #停用词
from nltk.stem import PorterStemmer    #词干提取
from nltk.stem import WordNetLemmatizer    #词性还原

paragraph = "I went to the gymnasium yesterday  , when I had finished my homework !".lower()
cutwords1 = word_tokenize(paragraph)   #分词

interpunctuations = [',', ' ','.', ':', ';', '?', '(', ')', '[', ']', '&', '!', '*', '@', '#', '$', '%', '^']   #定义符号列表
cutwords2 = [word for word in cutwords1 if word not in interpunctuations]   #去除标点符号

stops = set(stopwords.words("english"))
cutwords3 = [word for word in cutwords2 if word not in stops]  #判断分词在不在停用词列表内

print(pos_tag(cutwords3))      #词性标注

cutwords4 = []
for cutword1 in cutwords3:
    cutwords4.append(PorterStemmer().stem(cutword1))    #词干提取

cutwords5 = []
for cutword2 in cutwords4:
    cutwords5.append(WordNetLemmatizer().lemmatize(cutword2,pos='v'))   #指定还原词性为名词
print(cutwords5)
